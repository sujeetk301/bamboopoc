package TestScripts;

import BaseClass.BaseTest;
import BaseClass.TestFunctionality;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class SalesforceTest extends BaseTest {


    @Test
    public void SalesforceCreateAccount() throws InterruptedException {

        TestFunctionality tf = new TestFunctionality();
        tf.login(driver);
        tf.checkPageIsReady(driver);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//one-app-launcher-header/button/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input)[4]")).sendKeys("accounts");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='Account']/div//b")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@title=\"New\"]/div")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//article/div[3]/div/div[1]/div/div/div[2]/div[1]//input")).sendKeys(tf.randomString(8));
        driver.findElement(By.xpath("(//input[@type='tel'])[1]")).sendKeys(tf.phoneNumber());
        driver.findElement(By.xpath("(//span[text()='Save'])[2]")).click();
        Thread.sleep(1000);

    }



}
