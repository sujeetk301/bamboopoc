package BaseClass;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import java.text.DecimalFormat;
import java.util.Random;

public class TestFunctionality {

    public void login(WebDriver driver) throws InterruptedException {
        driver.get("https://resourceful-wolf-m5mc1p-dev-ed.my.salesforce.com/");
        AssertJUnit.assertEquals(driver.getTitle(), "Login | Salesforce");
        driver.findElement(By.id("username")).sendKeys("sujeet.kumar379951@gmail.com");
        driver.findElement(By.id("password")).sendKeys("Dreamforce2");
        driver.findElement(By.id("Login")).click();

        Thread.sleep(5000);
    }

    public String phoneNumber(){

        Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

        String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);

        return phoneNumber;
    }

    public String randomString(int length) {
        // create a string of all characters
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // create random string builder
        StringBuilder sb = new StringBuilder();
        // create an object of Random class
        Random random = new Random();
        // specify length of random string

        for (int i = 0; i < length; i++) {
            // generate random index number
            int index = random.nextInt(alphabet.length());
            // get character specified by index
            // from the string
            char randomChar = alphabet.charAt(index);
            // append the character to string builder
            sb.append(randomChar);
        }
        return sb.toString();
    }

    public void checkPageIsReady(WebDriver driver) {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        //Initially bellow given if condition will check ready state of page.
        if (js.executeScript("return document.readyState").toString().equals("complete")) {
            return;
        }
    }

}
