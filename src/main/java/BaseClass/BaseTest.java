package BaseClass;

import okio.Buffer;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BaseTest {

    protected WebDriver driver;
    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Starting Test On Chrome Browser");
        System.setProperty("webdriver.chrome.driver", "D:/Softwares/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

    }

    @AfterMethod
    public void afterMethod() {

        driver.close();
        System.out.println("Finished Test On Chrome Browser");
        }

}